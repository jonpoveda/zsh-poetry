# zsh-poetry

Poetry's zsh autocompletion

## Installation

### zinit

As a plugin

```sh
zinit ice pick"poetry.zsh" from"gitlab"
zinit light jonpoveda/zsh-poetry
```

or just the completions

```sh
zinit ice as"completion"
zinit snippet https://gitlab.com/jonpoveda/zsh-poetry/-/raw/master/_poetry
```
